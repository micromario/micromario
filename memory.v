`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 30.08.2018 11:17:48
// Design Name: 
// Module Name: memory
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module memory(
    input wire clk,
    input wire [13:0] addr_next,
    input wire [11:0] addr_map_next,
    input wire [13:0] mario_addr_next,
    input wire [13:0] mush_addr_next,
    input wire [15:0] addr_start_page,
    input wire [12:0] addr_game_over_page,
    input wire [12:0] addr_won_page,
    input wire en_map,
    input wire en_sprite1,
    input wire en_sprite2,
    input wire en_start_page,
    input wire en_game_over_page,
    input wire en_won_page,
    
    output wire [4:0] sprite_block,
    output wire [7:0] sprite1_out,
    output wire [7:0] sprite2_out,
    output wire [7:0] mario_char_out,
    output wire [7:0] mush_char_out,
    output wire [7:0] start_page_out,
    output wire [7:0] game_over_page_out,
    output wire [7:0] won_page_out
    );
    
    mapa mapa(
        .clk(clk),
        .en(en_map), 
        .addr(addr_map_next), 
        .dataout(sprite_block)
    );
        
    sprite1 sprite1(
        .clk(clk), 
        .en(en_sprite1), 
        .addr(addr_next),
        .addr_mario(mario_addr_next),
        .addr_mush(mush_addr_next),         
         
        .dataout(sprite1_out), 
        .dataout_mario(mario_char_out),
        .dataout_mush(mush_char_out)
    );
    
    sprite2 sprite2(
        .clk(clk), 
        .en(en_sprite2), 
        .addr(addr_next), 
        .dataout(sprite2_out)
    );
    
    start_page start_page(
        .clk(clk), 
        .en(en_start_page), 
        .addr(addr_start_page), 
        .dataout(start_page_out)
    );
    
    game_over_page game_over_page(
        .clk(clk),
        .en(en_game_over_page),
        .addr(addr_game_over_page),
        .dataout(game_over_page_out)
    );
    
    won_page won_page(
        .clk(clk),                  
        .en(en_won_page),     
        .addr(addr_won_page), 
        .dataout(won_page_out)        
    );
    
endmodule
