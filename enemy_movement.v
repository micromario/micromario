`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05.09.2018 08:02:59
// Design Name: 
// Module Name: enemy_movement
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module enemy_movement(
    input wire clk,
    input wire reset,
    input wire [10:0] pix_x, pix_y,
    input wire game_on,
    
    output wire [9:0] enemy_x,
    output wire [9:0] enemy_y,
    output wire enemy_on,
    output wire [13:0] enemy_addr
    );
    
    // refr_tick: 1-clock tick asserted at start of v-sync
    //            i.e., when the screen is refreshed (60Hz) 
    wire ref_tick;
    assign ref_tick = (pix_y==640) && (pix_x==800);
    
    //boundary
    localparam ENEMY_X_SIZE = 32;
    //enemy left, right boundary
    wire [9:0] enemy_x_left, enemy_x_right;
    //enemy top, bottom boundary
    localparam ENEMY_Y_TOP = 480;
    localparam ENEMY_Y_BOTTOM = 511;
    //reg to track left position (y is fixed)
    reg [9:0] enemy_x_reg;
    wire [9:0] enemy_x_next;
    //reg to track enemy speed
    reg [9:0] x_delta_reg, x_delta_next;
    //enemy velocity
    localparam ENEMY_V_R = 2;
    localparam ENEMY_V_L = -1;
    
    assign enemy_x_left = enemy_x_reg;
    assign enemy_x_right = enemy_x_left + ENEMY_X_SIZE -1;
    
    assign enemy_on = (enemy_x_left<=pix_x) && (enemy_x_right>=pix_x) &&
                      (ENEMY_Y_TOP<=pix_y) && (ENEMY_Y_BOTTOM>=pix_y);
    //new enemy position
    assign enemy_x_next = (ref_tick && game_on) ? enemy_x_reg + x_delta_reg : enemy_x_reg;
    //new enemy velocity
    always @*
    begin
        x_delta_next = x_delta_reg;
        if (enemy_x_left < 1) //reach left end of screen
            x_delta_next <= ENEMY_V_R;
        else if (enemy_x_right > 799) //reach right end of screen
            x_delta_next <= ENEMY_V_L;    
    end
    
    always @(posedge clk)
        if(reset) begin
            enemy_x_reg <= 768;
            x_delta_reg <= 2;
        end else begin
            enemy_x_reg <= enemy_x_next; 
            x_delta_reg <= x_delta_next;
        end
        
    assign enemy_addr = ((pix_x-enemy_x_left) % 32)+(((pix_y-ENEMY_Y_TOP)%32)<<5);
    
    assign enemy_x = enemy_x_left;
    assign enemy_y = ENEMY_Y_TOP;
    
endmodule
