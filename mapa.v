`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.08.2018 09:17:30
// Design Name: 
// Module Name: mapa
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module mapa
	#(parameter
		ADDR_WIDTH = 12,
		DATA_WIDTH = 5
	)
	(
		input wire clk, // posedge active clock
		input wire en,  // enable, can be removed if not needed
		input wire [ADDR_WIDTH - 1 : 0 ] addr,
		output reg [DATA_WIDTH - 1 : 0 ] dataout
	);

	(* rom_style = "block" *) // block || distributed

	reg [DATA_WIDTH-1:0] mapa [2**ADDR_WIDTH-1:0]; // rom memory

	initial
		$readmemb("mapa.bin", mapa, 0, 2499);

	always @(posedge clk) begin
		if (en)
			dataout <= mapa[addr];
	end

endmodule