`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 20.08.2018 18:06:51
// Design Name: 
// Module Name: buffor
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module bitmap_gen(
	input wire clk, reset,
	input wire video_on,
	input wire [10:0] pix_x, pix_y,
	input wire [4:0] keyboard_flags, //{None,W,A,D,Enter}
	input wire game_on,
	
	output reg [7:0] bit_rgb
	);
	
	parameter TRANSPARENT_COLOR = 8'b11010111;
    parameter SKY_COLOR = 8'b11100000;
	
	// Memory address management
	wire [13:0] addr_next;
	wire [13:0] mario_addr;                  
	wire [13:0] mush_addr;                  
	wire [11:0] addr_map_next;
	wire sum1, sum2;
	wire [15:0] addr_start_page;
	wire [12:0] addr_game_over_page;
	wire [12:0] addr_won_page;
	
	// Scrolling the map
	reg [7:0] shift_map;
	wire [7:0] shift_map_next;
	wire mapa_right, mapa_left;   
	
	// Current values of sprite1(pixel), sprite2(pixel), mario(pixel), mush(pixel), start_page(pixel),
	// bame_over_page(pixel), won_page(pixel), map(block)
	wire [7:0] sprite1_out, sprite2_out, mario_char_out, mush_char_out, start_page_out, game_over_page_out, won_page_out;
	wire [4:0] sprite_block;
	
	//memory of pixel's pages, map's blocks
	memory memory(
	   .clk(clk),
	   .addr_map_next(addr_map_next),
	   .addr_next(addr_next),
	   .mario_addr_next(mario_addr),
	   .mush_addr_next(mush_addr),
	   .addr_start_page(addr_start_page),
	   .addr_game_over_page(addr_game_over_page),
	   .addr_won_page(addr_won_page),
	   .en_map(1'b1),       
       .en_sprite1(1'b1),   
       .en_sprite2(1'b1),   
       .en_start_page(1'b1),
       .en_game_over_page(1'b1),
       .en_won_page(1'b1),
	   
	   .sprite_block(sprite_block),  
       .sprite1_out(sprite1_out),   
       .sprite2_out(sprite2_out),   
       .mario_char_out(mario_char_out),
       .mush_char_out(mush_char_out),
       .start_page_out(start_page_out), 
       .game_over_page_out(game_over_page_out), 
       .won_page_out(won_page_out) 
	);
    
	//---------------------------------
	// Calculation of memory positions
	//---------------------------------
	assign addr_map_next = shift_map*20+((pix_x >>5)*20)+(pix_y>>5);
	assign addr_next = (pix_x % 32)+((pix_y%32)<<5)+(sprite_block-1)*32*32;
	assign addr_start_page = ((pix_x-271)%256) + (((pix_y-99)%256)<<8);
	assign addr_game_over_page = ((pix_x-336)%128) + (((pix_y-280)%64)<<7);
	assign addr_won_page = ((pix_x-336)%128) + (((pix_y-280)%64)<<7);
	
	// Limit for the shift
	assign sum1 = shift_map < 100? mapa_right:0;
	assign sum2 = shift_map > 0? mapa_left:0;
	
	assign shift_map_next = shift_map + sum1 - sum2; 	
	
	always @(posedge clk) 
		if (reset) begin
			shift_map <= 0;
		end else begin
			shift_map <= shift_map_next;
		end
	
	
	//---------------------------------
	//Mario
	//---------------------------------	
	wire mario_on;
	wire [9:0] mario_x, mario_y;
		
    player_movement mario(
        .clk(clk),
        .reset(reset),
        .key_up(keyboard_flags[1]),
        .key_left(keyboard_flags[2]),
        .key_right(keyboard_flags[3]),
        .pix_x(pix_x),
        .pix_y(pix_y),
        .game_on(game_on),
        .shift_map(shift_map),
        
        .mario_x(mario_x),
        .mario_y(mario_y),
        .mario_on(mario_on),
        .mario_addr(mario_addr),
        .mapa_right(mapa_right),
        .mapa_left(mapa_left)
    );
	
    //mario rgb output
    wire [7:0] mario_rgb;
    assign mario_rgb = mario_char_out;	
    
    
    //---------------------------------
    //mush
    //---------------------------------
    wire mush_on;
    wire [9:0] mush_x, mush_y;
    
    enemy_movement mush(
        .clk(clk),
        .reset(reset),
        .pix_x(pix_x),
        .pix_y(pix_y),
        .game_on(game_on),
        
        .enemy_x(mush_x),
        .enemy_y(mush_y),
        .enemy_on(mush_on),
        .enemy_addr(mush_addr)
    );
    
    //mush rgb output
    wire [7:0] mush_rgb;
    assign mush_rgb = mush_char_out;
    
    //----------------------------------
    //game over
    //----------------------------------
    wire game_over;
    
    game_over_control game_over_ctl(
        .clk(clk),
        .reset(reset),
        .xpos_mario_in(mario_x),
        .ypos_mario_in(mario_y),
        .xpos_mush_in(mush_x),
        .ypos_mush_in(mush_y),
        
        .game_over(game_over)
    );    

    wire [7:0] game_over_rgb;
    assign game_over_rgb = game_over_page_out;
    
    //----------------------------------
    //won
    //----------------------------------
    wire won;
    
    won_control won_ctl(
        .clk(clk),
        .reset(reset),
        .xpos_mario_in(mario_x),
        .shift_map(shift_map),
        
        .won(won)
    );
    
    wire [7:0] won_rgb;
    assign won_rgb = won_page_out;
    
	// Returned from the pixel
	wire is_sprite2, is_sky, is_start_page, is_mario, is_mush, is_game_over, is_won;
   
	assign is_sprite2	= (sprite_block > 16) && (!game_over || won);
	assign is_sky = ((sprite_block == 5'b00000) || (is_sprite2 && (sprite2_out == TRANSPARENT_COLOR)) || ((!is_sprite2) && sprite1_out == TRANSPARENT_COLOR) 
	                || (is_start_page && start_page_out==TRANSPARENT_COLOR) || (is_mario && mario_rgb==TRANSPARENT_COLOR) 
	                || (is_mush && mush_rgb==TRANSPARENT_COLOR) || (is_won && won_rgb==TRANSPARENT_COLOR)) && (!game_over || won);
	assign is_start_page = (pix_x > 271 & pix_x <= 528 & pix_y > 99 & pix_y <= 355)  && !game_on && (start_page_out!=TRANSPARENT_COLOR) && !game_over;
	assign is_mario = mario_on  && (mario_rgb!=TRANSPARENT_COLOR) && (!game_over || won);
	assign is_mush = mush_on  && (mush_rgb!=TRANSPARENT_COLOR) && (!game_over || won);
	assign is_game_over = game_over  && (game_over_rgb!=TRANSPARENT_COLOR) && !won;
	assign is_won = won && (won_rgb!=TRANSPARENT_COLOR) && 336<pix_x && 463>=pix_x && 280<pix_y && 343>=pix_y;
	
	always @*
		begin
		bit_rgb = 8'b00000000;
		if (video_on)
			begin
			    if (is_mario)
                    bit_rgb = mario_rgb;
                else if (is_start_page)
                    bit_rgb = start_page_out;
                else if (is_mush)
                    bit_rgb = mush_rgb;
                else if (is_won)
                    bit_rgb = won_rgb;
			    else if (is_sky)
                    // Block value 0 shows the blue sky
                    bit_rgb = SKY_COLOR;
                else if (is_game_over)
                    if (336<pix_x && 463>=pix_x && 280<pix_y && 343>=pix_y)
                        bit_rgb = game_over_rgb;
                    else
                        bit_rgb = 8'b00000000;  			    				
				else if (is_sprite2)
					bit_rgb = sprite2_out;
				else	
					bit_rgb = sprite1_out;
			end
		end	

endmodule
