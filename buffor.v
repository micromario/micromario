`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02.09.2018 11:46:24
// Design Name: 
// Module Name: buffor
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module buffor(
    input wire clk,
    input wire reset,
    input wire [25:0] data_in,
    
    output reg [25:0] data_out
    );
    
    always @(posedge clk)
        if(reset)
            data_out<=26'd0;
        else
            data_out<=data_in;
    
endmodule
