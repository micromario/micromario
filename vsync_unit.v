`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 20.08.2018 13:31:09
// Design Name: 
// Module Name: vga_sync
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// Modu� zaimplementowany z ksi��ki FPGA Prototyping with Verilog examples
//////////////////////////////////////////////////////////////////////////////////
module vga_sync(
    input wire clk, reset,
	output wire hsync, vsync, video_on, p_tick,
	output wire [10:0] pixel_x, pixel_y
);

`define R800x640
//`define R640x480 


`ifdef R640x480
localparam HD=640;	//
localparam VD=480;	//
localparam tamCont=2;
`endif

`ifdef R800x640
localparam HD=800;	//
localparam VD=640;	//
localparam tamCont=1;
`endif
//Declarations of variables
//Parameters for vertical and horizontal synchronization.

localparam HF=48;		//left border
localparam HB=16;		//right border
localparam HR=96;		//horizontal retrace 
localparam VF=10;		//top border
localparam VB=33;		//bottom border
localparam VR=2;		//vertical retrace

//mod_2 counter (Counter for the frequency divider)
reg [tamCont-1:0] mod2_reg;
reg [tamCont-1:0] mod2_next;
//sync counters 
reg [10:0] h_count_reg, h_count_next;
reg [10:0] v_count_reg, v_count_next;
//output buffer
reg v_sync_reg, h_sync_reg;
wire v_sync_next, h_sync_next;
//status signal
wire h_end, v_end;
reg pixel_tick_next, pixel_tick;
//body
//registers
always @(posedge clk)
	if(reset)
	begin
		mod2_reg <= {tamCont{1'b0}};
		v_count_reg <= 0;
		h_count_reg <= 0;
		v_sync_reg <= 1'b0;
		h_sync_reg <= 1'b0;
		pixel_tick <= 1'b0;
	end
	else
	begin
		pixel_tick <= pixel_tick_next;
		mod2_reg <= mod2_next;
		v_count_reg <= v_count_next;
		h_count_reg <= h_count_next;
		v_sync_reg <= v_sync_next;
		h_sync_reg <= h_sync_next;
	end
//mod_2 circuit to generate 25MHz enable tick
always @* 
	begin
	mod2_next = mod2_reg + 1'b1;
	if(mod2_reg == {tamCont{1'b0}}) 
		pixel_tick_next = 1'b1;
	else
		pixel_tick_next = 1'b0;
	end
//status signals
//end of horizontal counter (959)
assign h_end = (h_count_reg == (HD+HF+HB+HR-1));
//end of vertical counter (684)
assign v_end = (v_count_reg == (VD+VF+VB+VR-1));
//next_state logic of mod_960 horizontal sync counter
always @*
	if(pixel_tick)
	//50MHz pulse
		if(h_end)
			h_count_next = 0;
		else
			h_count_next = h_count_reg + 1'b1;
	else
		h_count_next = h_count_reg;
//next_state logic of mod_685 vertical sync counter
always @*
	if(pixel_tick & h_end)
		if(v_end)
			v_count_next = 0;
		else
			v_count_next = v_count_reg + 1'b1;
	else
		v_count_next = v_count_reg;
//horizontal and vertical sync, buffered to avoid glitch
//h_sync_next asserted between 816 and 911
assign h_sync_next = (h_count_reg >= (HD+HB) && h_count_reg <= (HD+HB+HR-1));
//vh_sync_next asserted between 673 and 674
assign v_sync_next = (v_count_reg >= (VD+VB)&& v_count_reg <= (VD+VB+VR-1));

//video on/off
assign video_on = (h_count_reg < HD) && (v_count_reg < VD);
//output
assign hsync = h_sync_reg;
assign vsync = v_sync_reg;
assign pixel_x = (h_count_reg > HF & h_count_reg <= (HD+HF) & v_count_reg > VF & v_count_reg <= (VD+VF)) ? h_count_reg - HF : {11{1'b0}};
assign pixel_y = (v_count_reg > VF & v_count_reg <= (VD+VF)) ? v_count_reg - VF : {11{1'b0}};
assign p_tick = (h_count_reg > HF & h_count_reg <= (HD+HF) & v_count_reg > VF & v_count_reg <= (VD+VF)) ? pixel_tick : 1'b0;


endmodule