`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 25.08.2018 18:33:31
// Design Name: 
// Module Name: start_page
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module start_page
	#(parameter
		ADDR_WIDTH = 16,
		DATA_WIDTH = 8
	)
	(
		input wire clk, // posedge active clock
		input wire en,  // enable, can be removed if not needed
		input wire [ADDR_WIDTH - 1 : 0 ] addr,
		output reg [DATA_WIDTH - 1 : 0 ] dataout
	);

	(* rom_style = "block" *) // block || distributed

	reg [DATA_WIDTH-1:0] image [2**ADDR_WIDTH-1:0]; // rom memory

	initial
		$readmemb("mario_start_page.bin", image, 0, (2**ADDR_WIDTH)-1);

	always @(posedge clk) begin
		if (en)
			dataout <= image[addr];
	end

endmodule
