`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05.09.2018 12:18:35
// Design Name: 
// Module Name: won_control
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module won_control(
    input wire clk,
    input wire reset,    
    input wire [9:0] xpos_mario_in, 
    input wire [7:0] shift_map,   
       
    output reg won 
);
    
    reg won_next = 1'b0;
    
    always @(posedge clk)
    if (reset)
        begin
            won <= 1'b0;
        end
    else 
        begin   
            won <= won_next;   
        end
    
    always @(*) 
    begin    
    
    if (xpos_mario_in > 703 && shift_map == 100)
        begin
            won_next <= 1'b1;
        end
    else 
        if (won == 1'b1)
            begin
                won_next <= 1'b1;           
            end
        else
            begin
                won_next <= 1'b0;
            end
    end

endmodule