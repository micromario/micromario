`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05.09.2018 09:48:03
// Design Name: 
// Module Name: game_over_control
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module game_over_control(
    input wire clk,
    input wire reset,    
    input wire [9:0] xpos_mario_in,
    input wire [9:0] ypos_mario_in,
    input wire [9:0] xpos_mush_in,
    input wire [9:0] ypos_mush_in,    
       
    output reg game_over 
);

reg game_over_next = 1'b0;

always @(posedge clk)
    if (reset)
        begin
            game_over <= 1'b0;
        end
    else 
        begin   
            game_over <= game_over_next;   
        end

always @(*) 
begin    
 game_over_next <= 1'b0;
 
    if ((xpos_mario_in +31 >= xpos_mush_in && xpos_mario_in <= 31 +xpos_mush_in) && (ypos_mario_in +31 >= ypos_mush_in))
        begin
            game_over_next <= 1'b1;
        end
    else 
        if (game_over == 1'b1)
            begin
                game_over_next <= 1'b1;           
            end
        else
            begin
                game_over_next <= 1'b0;
            end
end

endmodule
