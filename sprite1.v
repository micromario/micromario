`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.08.2018 09:49:06
// Design Name: 
// Module Name: sprite1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module sprite1
	#(parameter
		ADDR_WIDTH = 14,
		DATA_WIDTH = 8
	)
	(
		input wire clk, // posedge active clock
		input wire en,  // enable, can be removed if not needed
		input wire [ADDR_WIDTH - 1 : 0 ] addr,
		input wire [ADDR_WIDTH - 1 : 0 ] addr_mario,
		input wire [ADDR_WIDTH - 1 : 0 ] addr_mush,
		output reg [DATA_WIDTH - 1 : 0 ] dataout,
		output reg [DATA_WIDTH - 1 : 0 ] dataout_mario,
		output reg [DATA_WIDTH - 1 : 0 ] dataout_mush
	);

	(* rom_style = "block" *) // block || distributed

	reg [DATA_WIDTH-1:0] image [2**ADDR_WIDTH-1:0]; // rom memory

	initial
		$readmemb("sprite1.bin", image, 0, (2**ADDR_WIDTH)-1);

	always @(posedge clk) begin
		if (en)
			dataout <= image[addr];
			dataout_mario <= image[addr_mario];
			dataout_mush <= image[addr_mush];
	end

endmodule