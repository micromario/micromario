`timescale 1ns / 1ps

//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 20.08.2018 11:31:09
// Design Name: 
// Module Name: mario
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//////////////////////////////////////////////////////////////////////////////////

module mario(
	input wire clk, reset,
	input wire PS2_clk,
	input wire PS2_data,

	output wire hsync, vsync,
	output wire [7:0] rgb
    );
	
	//clocks 
	wire clk_100MHz, clk_50MHz; 
	//VGA signals
	wire [10:0] pixel_x, pixel_y;
	wire video_on, pixel_tick;
	//rgb connector
	reg [7:0] rgb_reg;
	wire [7:0] rgb_next;
	//keyboard flags
	wire [4:0] keyboard_flags; //{None,W,A,D,Enter}
	//on/off game
	wire game_on;
	
	clk_div clk_div (
	   .clk(clk),
	   .reset(reset),
	   
	   .clk_100MHz(clk_100MHz),
	   .clk_50MHz(clk_50MHz)
	);
	
	wire [25:0] buffor_in;
	
	vga_sync vga_unit(
		.clk(clk_100MHz), 
		.reset(reset),
		 
		.hsync(buffor_in[0]), 
		.vsync(buffor_in[1]),
		.video_on(buffor_in[2]), 
		.p_tick(buffor_in[3]),
		.pixel_x(buffor_in[14:4]), 
		.pixel_y(buffor_in[25:15])
	);
	
	buffor buffor_vsync(
	   .clk(clk_50MHz),
	   .reset(reset),
	   .data_in(buffor_in),
	   .data_out({pixel_y,pixel_x,pixel_tick,video_on,vsync,hsync})
	);
		
	bitmap_gen bitmap_unit(
		.clk(clk_50MHz), 
		.reset(reset), 
		.video_on(video_on), 
		.pix_x(pixel_x),
		.pix_y(pixel_y),
		.keyboard_flags(keyboard_flags), 
		.game_on(game_on),
		
		.bit_rgb(rgb_next)
	);
		
    keyboard keyboard_manager(
        .clk(clk_50MHz),
        .reset(reset),
        .kclk(PS2_clk),
        .kdata(PS2_data),
        
        .oflag(),
        .k_code_out(),
        .key_flag_None(keyboard_flags[0]),
        .key_flag_W(keyboard_flags[1]),   
        .key_flag_A(keyboard_flags[2]),   
        .key_flag_D(keyboard_flags[3]),   
        .key_flag_Enter(keyboard_flags[4])
    );
    
    start_control start_ctl(
        .clk(clk_50MHz),
        .reset(reset),
        .flag_Enter(keyboard_flags[4]),
        
        .game_on(game_on)        
    );
		
	always @(posedge clk_50MHz)
	    if (reset)
	             rgb_reg <=  'b0;
		else if (pixel_tick)
			     rgb_reg <=  rgb_next;
	
	assign rgb = rgb_reg;


endmodule
