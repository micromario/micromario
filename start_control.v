`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 25.08.2018 17:46:28
// Design Name: 
// Module Name: start_control
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module start_control(
    input wire clk,
    input wire reset,
    input wire flag_Enter,
    
    output wire game_on    
    );
    
    reg game_on_next = 1'd0;
    reg game_on_reg = 1'd0;
    
    always @(posedge clk)
        if(reset)
            game_on_reg <= 1'd0;
        else
            game_on_reg <= game_on_next;
            
    always @*
        if(flag_Enter)
            game_on_next <= 1'd1;
        else
            if(!game_on_reg)
                game_on_next <= 1'd0;
            else
                game_on_next <= 1'd1;
                
    assign game_on = game_on_reg;
    
endmodule
