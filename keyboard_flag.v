`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 13.05.2018 15:22:58
// Design Name: 
// Module Name: keyboard_flag
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module keyboard_flag(
    input wire clk,
    input wire reset,
    input wire [15:0] k_code,
    
    output wire [15:0] k_code_out,
    output reg key_flag_None,
    output reg key_flag_W,
    output reg key_flag_A,
    output reg key_flag_D,
    output reg key_flag_Enter
    );            
    
assign k_code_out = k_code;

reg key_flag_None_nxt=0; 
reg key_flag_W_nxt=0;      
reg key_flag_A_nxt=0;    
reg key_flag_D_nxt=0;    
reg key_flag_Enter_nxt=0; 

    
always @(posedge clk) begin
    if (reset) begin
    key_flag_None   <= 1;
    key_flag_W      <= 0;
    key_flag_A      <= 0;
    key_flag_D      <= 0;
    key_flag_Enter  <= 0;
    end else begin
    key_flag_None   <= key_flag_None_nxt; 
    key_flag_W      <= key_flag_W_nxt;    
    key_flag_A      <= key_flag_A_nxt;    
    key_flag_D      <= key_flag_D_nxt;    
    key_flag_Enter  <= key_flag_Enter_nxt;
    end
end

always @* begin             
    key_flag_None_nxt   <= 1;  //defaults
    key_flag_W_nxt      <= 0;
    key_flag_A_nxt      <= 0;
    key_flag_D_nxt      <= 0;
    key_flag_Enter_nxt  <= 0;    
    
    casex (k_code)          
    16'hF0xx: key_flag_None_nxt <= 1;
    16'hxx1D: begin key_flag_W_nxt <= 1; key_flag_None_nxt <= 0; end
    16'hxx1C: begin key_flag_A_nxt <= 1; key_flag_None_nxt <= 0; end
    16'hxx23: begin key_flag_D_nxt <= 1; key_flag_None_nxt <= 0; end
    16'hxx5A: begin key_flag_Enter_nxt <= 1; key_flag_None_nxt <= 0; end
    endcase                     
end  
    
endmodule
