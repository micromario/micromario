`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03.09.2018 22:11:47
// Design Name: 
// Module Name: player_movement
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module player_movement(
    input wire clk,
    input wire reset,
    input wire key_up, key_left, key_right,
    input wire [10:0] pix_x, pix_y,
    input wire game_on,
    input wire [7:0] shift_map,
    
    output wire [9:0] mario_x,    
    output wire [9:0] mario_y,    
    output wire mario_on,
    output wire [13:0] mario_addr,
    output wire mapa_right,
    output wire mapa_left
    );
    
	// refr_tick: 1-clock tick asserted at start of v-sync
    //            i.e., when the screen is refreshed (60Hz) 
    wire ref_tick;
    assign ref_tick = (pix_y==640) && (pix_x==800);
    
    //mapa moving
    reg mapa_right_next, mapa_left_next;
    wire mapa_right_wire, mapa_left_wire;

    
    //---------------------------------
    //Mario player
    //---------------------------------
    //mario left, right boundary
    wire [9:0] mario_x_left, mario_x_right;
    localparam MARIO_X_SIZE = 32;
    //mario top, bottom boundary
    wire [9:0] mario_y_top, mario_y_bottom;
    localparam MARIO_Y_SIZE = 32;
    localparam MARIO_Y_TOP_START = 480;
    //register to track mario boundary
    reg [9:0] mario_x_reg, mario_x_next;
    reg [9:0] mario_y_reg, mario_y_next;
    // mario moving velocity when button is pressed
    localparam MARIO_V_hor = 1;
    localparam MARIO_V_ver = 2;
    
    //boundary
    assign mario_x_left =  mario_x_reg;
    assign mario_x_right = mario_x_left + MARIO_X_SIZE - 1;
    assign mario_y_top = mario_y_reg;
    assign mario_y_bottom = mario_y_top + MARIO_Y_SIZE - 1;
    //pixel within mario
    assign mario_on = ((mario_x_left<=pix_x) && (pix_x<=mario_x_right) &&
                      (mario_y_top<=pix_y) && (pix_y<=mario_y_bottom));

    //mario direction
    wire up, down, left, right;
    assign up = key_up && (mario_y_top >= 256 + MARIO_V_ver) && (mario_y_top <= MARIO_Y_TOP_START);
    assign down = (mario_y_top >= 256) && (mario_y_top < MARIO_Y_TOP_START);
    assign left = key_left && (mario_x_left > MARIO_V_hor);
    assign right = key_right && (mario_x_right < (800 - 1 - MARIO_X_SIZE - MARIO_V_hor));
    //new mario position
    always @*
    begin
        mario_x_next = mario_x_reg; //no move
        mario_y_next = mario_y_reg; //no move
        mapa_right_next = 0;
        mapa_left_next = 0;
        if (ref_tick && game_on)
            casex({up,down,left,right,mapa_left_wire,mapa_right_wire})
            6'b1xxxxx: mario_y_next = mario_y_reg - MARIO_V_ver; //up
            6'bx11xxx: begin mario_y_next = mario_y_reg + MARIO_V_ver; mario_x_next = mario_x_reg - MARIO_V_hor; end//down+left
            6'bx1x1xx: begin mario_y_next = mario_y_reg + MARIO_V_ver; mario_x_next = mario_x_reg + MARIO_V_hor; end//down+right
            6'bx1xxxx: mario_y_next = mario_y_reg + MARIO_V_ver; //down
            6'bxx1x0x: mario_x_next = mario_x_reg - MARIO_V_hor; //left
            6'bxx1x1x: begin mario_x_next = mario_x_reg - MARIO_V_hor +32; mapa_left_next = 1; end //left+mapa_left
            6'bxxx1x0: mario_x_next = mario_x_reg + MARIO_V_hor; //right
            6'bxxx1x1: begin mario_x_next = mario_x_reg + MARIO_V_hor -32; mapa_right_next = 1; end//right+mapa_right
            6'bxxxx1x: begin mario_x_next = mario_x_reg + 32; mapa_left_next = 1; end //mapa_left
            6'bxxxxx1: begin mario_x_next = mario_x_reg - 32; mapa_right_next = 1; end //mapa_right
        endcase
    end
    
    //addres to mario memory
    assign mario_addr = ((pix_x-mario_x_left) % 32)+(((pix_y-mario_y_top)%32)<<5)+32*32; 
    
    assign mapa_right_wire = mario_x_left>=544 && shift_map<100;
    assign mapa_left_wire = mario_x_left<=96 && shift_map>0;
    assign mapa_right = mapa_right_next;
    assign mapa_left = mapa_left_next;
    assign mario_x = mario_x_left;
    assign mario_y = mario_y_top;
    
	always @(posedge clk)
        if (reset) begin
            mario_x_reg <= 0;
            mario_y_reg <= MARIO_Y_TOP_START;
        end else begin
            mario_x_reg <= mario_x_next;
            mario_y_reg <= mario_y_next;
        end
    
endmodule
