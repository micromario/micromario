`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 18.05.2018 21:43:06
// Design Name: 
// Module Name: keyboard
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module keyboard(
    input clk,
    input reset,
    input kclk, //keyboard clk
    input kdata, //keyboard data
    
    output wire oflag,
    output wire [15:0] k_code_out,
    output wire key_flag_None,
    output wire key_flag_W,
    output wire key_flag_A,
    output wire key_flag_D,
    output wire key_flag_Enter   
);

wire [15:0] keycode;

PS2_receiver PS2_receiver (
    .clk(clk),
    .kclk(kclk),
    .kdata(kdata),

    .keycode(keycode),
    .oflag(oflag)
);

keyboard_flag keyboard_flag(
    .clk(clk),
    .reset(reset),
    .k_code(keycode),
    
    .k_code_out(k_code_out),
    .key_flag_None(key_flag_None),
    .key_flag_W(key_flag_W),
    .key_flag_A(key_flag_A),
    .key_flag_D(key_flag_D),
    .key_flag_Enter(key_flag_Enter)
);

endmodule
